let prefix = "csoundapi_stub"

let () =
  let generate_ml, generate_c = (ref false, ref false) in
  let () =
    Arg.(
      parse
        [
          ("-ml", Set generate_ml, "Generate ML");
          ("-c", Set generate_c, "Generate C");
        ])
      (fun _ -> failwith "unexpected anonymous argument")
      "stubgen [-ml|-c]"
  in
  match (!generate_ml, !generate_c) with
  | false, false | true, true ->
      failwith "Exactly one of -ml and -c must be specified"
  | true, false ->
      Cstubs.write_ml ~concurrency:Cstubs.unlocked Format.std_formatter ~prefix
        (module Csoundapi_bindings.Def)
  | false, true ->
      print_endline "#include <csound/csound.h>";
      Cstubs.write_c ~concurrency:Cstubs.unlocked Format.std_formatter ~prefix
        (module Csoundapi_bindings.Def)
