open Ctypes

module Def (F : Cstubs.FOREIGN) = struct
  open F

  type csound = unit ptr

  let csound_t = ptr void

  let initialize = foreign "csoundInitialize" (int @-> returning int)

  let create = foreign "csoundCreate" (ptr void @-> returning csound_t)

  let destroy = foreign "csoundDestroy" (csound_t @-> returning void)

  let compile_orc =
    foreign "csoundCompileOrc" (csound_t @-> string @-> returning int)

  let compile_orc_async =
    foreign "csoundCompileOrcAsync" (csound_t @-> string @-> returning int)

  let read_score =
    foreign "csoundReadScore" (csound_t @-> string @-> returning int)

  let perform = foreign "csoundPerform" (csound_t @-> returning int)

  let perform_ksmps = foreign "csoundPerformKsmps" (csound_t @-> returning int)

  let start = foreign "csoundStart" (csound_t @-> returning int)

  let stop = foreign "csoundStop" (csound_t @-> returning void)

  let reset = foreign "csoundReset" (csound_t @-> returning void)

  let clean_up = foreign "csoundCleanup" (csound_t @-> returning int)

  let set_option =
    foreign "csoundSetOption" (csound_t @-> string @-> returning int)

  let set_control_channel =
    foreign "csoundSetControlChannel"
      (csound_t @-> string @-> double @-> returning void)

  let get_control_channel =
    foreign "csoundGetControlChannel"
      (csound_t @-> string @-> ptr void @-> returning double)

  let score_event_async =
    foreign "csoundScoreEventAsync"
      (csound_t @-> char @-> ptr double @-> long @-> returning void)

  let score_event =
    foreign "csoundScoreEvent"
      (csound_t @-> char @-> ptr double @-> long @-> returning int)
end
