open Containers
open Signal.Export

let sink = Signal.Block.sink

let opc1 = Signal.opcode1

let opc2 = Signal.opcode2

let opc3 = Signal.opcode3

let opc4 = Signal.opcode4

let sine ?(rate = ar) ?(amp = ~$1.0) freq =
  match rate with
  | Ar -> opc1 "oscili" [ amp; freq ] rate
  | Kr -> opc1 "oscili" [ k amp; k freq ] rate
  | Ir -> failwith "unsupported"

let table_sub ?(wrap = ~$1.0) ?(rate = ar) ~index tab =
  opc1 "table3" [ to_rate rate index; i tab; ~$1.0; ~$0.0; i wrap ] rate

let triangle ?(amp = ~$1.0) freq = opc1 "vco2" [ k amp; k freq; ~$12.0 ] ar

let square ?(amp = ~$1.0) ?(pw = const 0.5) freq =
  opc1 "vco2" [ k amp; k freq; ~$2.0; k pw ] ar

let saw ?(amp = ~$1.0) freq = opc1 "vco2" [ k amp; k freq; ~$0. ] ar

let pan ?(pan = const 0.5) x = opc2 "pan2" [ a x; pan ] ar ar

let outs left right = sink "outs" [ a left; a right ]

let chnget name = sink1 "chnget" [ const_string name ] kr

let chnset name input = sink "chnset" [ const_string name; k input ]

let outp =
  let p = pan in
  let impl ?(pan = const 0.5) x =
    let left, right = p ~pan x in
    outs left right
  in
  impl

let hann ?(dur = edur) amp = sine (~$0.5 / dur) ~amp

let adsr ?(rate = kr) ?(delay = ~$0.0) ~att ~dec ~rel level =
  opc1 "adsr" [ i att; i dec; i level; i rel; i delay ] rate

let alpass ~reverb ~loop asig = opc1 "alpass" [ a asig; reverb; i loop ] ar

let areson ?(rate = ar) ~freq ~bw asig =
  match rate with
  | Kr -> opc1 "aresonk" [ k asig; k freq; k bw ] kr
  | Ar -> opc1 "areson" [ a asig; freq; bw ] ar
  | Ir -> failwith "unsupported"

let atone ?(rate = ar) ~hp asig =
  match rate with
  | Kr -> opc1 "atonek" [ k asig; k hp ] kr
  | Ar -> opc1 "atone" [ a asig; k hp ] ar
  | Ir -> failwith "unsupported"

let atonex ?(rate = ar) ?(num = ~$4.0) ~hp asig =
  match rate with
  | Ir -> failwith "unsupported"
  | _ -> opc1 "atonex" [ to_rate rate asig; to_rate rate hp; i num ] rate

let balance x y = opc1 "balance" [ a x; a y ] ar

let beosc ~bw freq = opc1 "beosc" [ freq; k bw ] ar

let betarand ?(rate = kr) ~range ~alpha ~beta () =
  opc1 "betarand" [ k range; k alpha; k beta ] rate

let bexprnd ?(rate = kr) range = opc1 "betarnd" [ k range ] rate

let biquad ?(inrate = kr) asig b0 b1 b2 a0 a1 a2 =
  match inrate with
  | Kr -> opc1 "biquad" [ a asig; k b0; k b1; k b2; k a0; k a1; k a2 ] ar
  | Ar -> opc1 "biquada" [ a asig; a b0; a b1; a b2; a a0; a a1; a a2 ] ar
  | Ir -> failwith "unsupported"

module Bqrez = struct
  let lp = 0.0

  let hp = 1.0

  let bp = 2.0

  let br = 3.0

  let ap = 4.0
end

let bqrez ?(mode = ~$Bqrez.lp) ?(res = ~$1.0) ~freq asig =
  opc1 "bqrez" [ a asig; freq; res; mode ] ar

let butbp ~freq ~bw asig = opc1 "butterbp" [ a asig; freq; bw ] ar

let butbr ~freq ~bw asig = opc1 "butterbr" [ a asig; freq; bw ] ar

let buthp ~freq asig = opc1 "butterhp" [ a asig; freq ] ar

let butlp ~freq asig = opc1 "butterlp" [ a asig; freq ] ar

let cauchy ?(rate = kr) alpha = opc1 "cauchy" [ k alpha ] rate

let cauchyi ?(rate = kr) ?(amp = ~$1.0) ~freq alpha =
  opc1 "cauchyi" [ k alpha; amp; freq ] rate

let centroid ~trig ~fftn asig = opc1 "centroid" [ a asig; k trig; i fftn ] kr

let changed ?(report_first = false) xs =
  opc1 (if report_first then "changed2" else "changed") (List.map k xs) kr

let chebyshevpoly k0 kn ain =
  opc1 "chebyshevpoly" (a ain :: k k0 :: List.map k kn) ar

let clip ~limit asig = opc1 "clip" [ a asig; ~$2.0; i limit ] ar

let comb ~reverb ~loop asig = opc1 "comb" [ a asig; k reverb; i loop ] ar

let combinv ~reverb ~loop asig = opc1 "combinv" [ a asig; k reverb; i loop ] ar

let compress ~thresh ~lowknee ~hiknee ~ratio ~att ~rel ~lookahead ~control_sig
    asig =
  opc1 "compress2"
    [
      a asig;
      a control_sig;
      k thresh;
      k lowknee;
      k hiknee;
      k ratio;
      k att;
      k rel;
      i lookahead;
    ]
    ar

let cosseg ?(rate = kr) ?(init = ~$0.0) pairs =
  opc1 "cosseg"
    (i init :: List.fold_right (fun (a, b) acc -> i a :: i b :: acc) pairs [])
    rate

let dam ~thresh ~hiratio ~lowratio ~rise ~fall asig =
  opc1 "dam" [ a asig; k thresh; i hiratio; i lowratio; i rise; i fall ] ar

let dcblock ?(order = ~$128.0) ain = opc1 "dcblock2" [ a ain; i order ] ar

let diff ?(rate = ar) asig =
  match rate with
  | Ar -> opc1 "diff" [ a asig ] ar
  | Kr -> opc1 "diff" [ k asig ] kr
  | Ir -> failwith "unsupported"

let distort1 asig ~pregain ~postgain ~posshape ~negshape =
  opc1 "distort1"
    [ a asig; k pregain; k postgain; k posshape; k negshape; ~$1.0 ]
    ar

let doppler ain ~pos ~micpos = opc1 "doppler" [ a ain; k pos; k micpos ] ar

let downsamp ?(window = ~$0.0) ain = opc1 "downsamp" [ a ain; i window ] kr

let dust ?(rate = kr) ?(with_neg = false) ~amp dens =
  match rate with
  | Ir -> failwith "unsupported"
  | _ -> opc1 (if with_neg then "dust2" else "dust") [ k amp; k dens ] rate

let ephasor ~decayrate freq = opc2 "ephasor" [ k freq; k decayrate ] ar ar

let eqfil ~freq ~bw ~gain ain = opc1 "eqfil" [ a ain; k freq; k bw; k gain ] ar

let exciter ~freq ~ceil ~harm ~blend asig =
  opc1 "exciter" [ a asig; k freq; k ceil; k harm; k blend ] ar

let expcurve ~steepness index = opc1 "expcurve" [ k index; k steepness ] kr

let expon ~dur a b = opc1 "expon" [ i a; i dur; i b ] kr

let exprand ?(rate = kr) lam = opc1 "exprand" [ k lam ] rate

let exprandi ?(rate = kr) ~amp ~freq lam =
  opc1 "exprandi" [ k lam; amp; freq ] rate

let expseg ?(rate = kr) ?(init = ~$0.001) pairs =
  let o = match rate with Ar -> "expsega" | Kr | Ir -> "expseg" in
  opc1 o
    (i init :: List.fold_right (fun (a, b) acc -> i a :: i b :: acc) pairs [])
    rate

let flanger ~delay ~feedback asig =
  opc1 "flanger" [ a asig; a delay; k feedback ] ar

let fold ~incr asig = opc1 "fold" [ a asig; k incr ] ar

let follow ~avgperiod asig = opc1 "follow" [ a asig; i avgperiod ] ar

let follow2 ~att ~rel asig = opc1 "follow2" [ a asig; k att; k rel ] ar

let fractal_noise ?(beta = ~$0.0) kamp =
  opc1 "fractalnoise" [ k kamp; k beta ] ar

let freeverb ?(sr = ~$44100.0) ~room_size ~hfdamp ainl ainr =
  opc2 "freeverb" [ a ainl; a ainr; k room_size; k hfdamp; i sr ] ar ar

let gain ~rms asig = opc1 "gain" [ a asig; k rms ] ar

let gauss ?(rate = kr) range = opc1 "gauss" [ k range ] rate

let gaussi ?(rate = kr) ~amp ~freq range =
  opc1 "gauss" [ k range; amp; freq ] rate

let gausstrig ?(rate = kr) ?(amp = ~$1.0) freq dev =
  opc1 "gausstrig" [ k amp; k freq; k dev; ~$1.0; ~$1.0 ] rate

let gendy_linear = ~$0.0

let gendy_cauchy = ~$1.0

let gendy_logist = ~$2.0

let gendy_hyperbcos = ~$3.0

let gendy_arcsine = ~$4.0

let gendy_expon = ~$5.0

let gendy_sinus = ~$6.0

let gendy ?(rate = kr) ?(amp = ~$1.0) ?ctrlpts ?(maxctrlpts = ~$12.0) ~ampdist
    ~durdist ~adpar ~ddpar ~minfreq ~maxfreq ~ascale ~dscale =
  opc1 "gendy"
    [
      k amp;
      k ampdist;
      k durdist;
      k adpar;
      k ddpar;
      k minfreq;
      k maxfreq;
      k ascale;
      k dscale;
      i maxctrlpts;
      k (Option.value ~default:maxctrlpts ctrlpts);
    ]
    rate

let gendyc ?(rate = kr) ?(amp = ~$1.0) ?ctrlpts ?(maxctrlpts = ~$12.0) ~ampdist
    ~durdist ~adpar ~ddpar ~minfreq ~maxfreq ~ascale ~dscale =
  opc1 "gendyc"
    [
      k amp;
      k ampdist;
      k durdist;
      k adpar;
      k ddpar;
      k minfreq;
      k maxfreq;
      k ascale;
      k dscale;
      i maxctrlpts;
      k (Option.value ~default:maxctrlpts ctrlpts);
    ]
    rate

let gendyx ?(rate = kr) ?(amp = ~$1.0) ?ctrlpts ?(maxctrlpts = ~$12.0) ~ampdist
    ~durdist ~adpar ~ddpar ~minfreq ~maxfreq ~ascale ~dscale ~curveup ~curvedown
    =
  opc1 "gendyx"
    [
      k amp;
      k ampdist;
      k durdist;
      k adpar;
      k ddpar;
      k minfreq;
      k maxfreq;
      k ascale;
      k dscale;
      k curveup;
      k curvedown;
      i maxctrlpts;
      k (Option.value ~default:maxctrlpts ctrlpts);
    ]
    rate

let hilbert asig = opc2 "hilbert" [ a asig ] ar ar

let hilbert2 ~fftsize ~hopsize asig =
  opc2 "hilbert2" [ a asig; i fftsize; i hopsize ] ar ar

let init rate arg = opc1 "init" [ i arg ] rate

let integ ?rate asig =
  let rate =
    match (rate, Signal.rate_opt_of_expr asig) with
    | None, None -> kr
    | Some r, None | None, Some r -> r
    | Some a, Some b when Signal.rate_equal a b -> a
    | _ -> failwith "unsupported"
  in
  opc1 "integ" [ asig ] rate

let interp ?init ksig =
  match init with
  | None -> opc1 "interp" [ k ksig; ~$0.0 ] ar
  | Some x -> opc1 "interp" [ k ksig; ~$0.0; ~$1.0; i x ] ar

let jitter ~minfreq ~maxfreq amp =
  opc1 "jitter" [ k amp; k minfreq; k maxfreq ] kr

let jitter2 ~comp1 ~comp2 ~comp3 amp =
  opc1 "jitte2"
    [
      k amp;
      k (fst comp1);
      k (snd comp1);
      k (fst comp2);
      k (snd comp2);
      k (fst comp3);
      k (snd comp3);
    ]
    kr

let jitter_spline ?(rate = kr) ~minfreq ~maxfreq amp =
  match rate with
  | Kr -> opc1 "jspline" [ k amp; k minfreq; k maxfreq ] kr
  | Ar -> opc1 "jspline" [ amp; k minfreq; k maxfreq ] ar
  | Ir -> failwith "unsupported"

let k35_hpf ~freq ~q ain = opc1 "K35_hpf" [ a ain; freq; q ] ar

let k35_lpf ~freq ~q ain = opc1 "K35_lpf" [ a ain; freq; q ] ar

module Lfo = struct
  let sine = 0.0

  let triangle = 1.0

  let square_bi = 2.0

  let square_uni = 3.0

  let saw = 4.0

  let saw_down = 5.0
end

let lfo ?(mode = ~$Lfo.sine) ?(rate = kr) ?(amp = ~$1.0) kcps =
  opc1 "lfo" [ k amp; k kcps; i mode ] rate

let limit ?rate ~low ~high inp =
  let rate =
    match (rate, Signal.rate_opt_of_expr inp) with
    | None, None -> ir
    | Some r, None | None, Some r -> r
    | Some a, Some b when Signal.rate_equal a b -> a
    | _ -> failwith "unsupported"
  in
  opc1 "limit" [ inp; k low; k high ] rate

let line ?(rate = kr) ?(dur = edur) a b = opc1 "line" [ i a; i dur; i b ] rate

let linen ?rate ?(dur = edur) ~rise ~decay amp =
  let rate =
    match (rate, Signal.rate_opt_of_expr amp) with
    | None, None -> kr
    | Some r, None | None, Some r -> r
    | Some a, Some b when Signal.rate_equal a b -> a
    | _ -> failwith "unsupported"
  in
  opc1 "linen" [ amp; i rise; i dur; i decay ] rate

let lineto ~time ksig = opc1 "lineto" [ k ksig; k time ] kr

let linlin ?rate ?(dst = (~$0.0, ~$1.0)) ~src:(a, b) x =
  let rate =
    match (rate, Signal.rate_opt_of_expr x) with
    | None, None -> ir
    | Some r, None | None, Some r -> r
    | Some a, Some b when Signal.rate_equal a b -> a
    | _ -> failwith "unsupported"
  in
  opc1 "linlin" [ x; a; b; fst dst; snd dst ] rate

let lincos ?rate ?(dst = (~$0.0, ~$1.0)) ~src:(a, b) x =
  let rate =
    match (rate, Signal.rate_opt_of_expr x) with
    | None, None -> ir
    | Some r, None | None, Some r -> r
    | Some a, Some b when Signal.rate_equal a b -> a
    | _ -> failwith "unsupported"
  in
  opc1 "lincos" [ x; a; b; fst dst; snd dst ] rate

let linrand ?(rate = kr) max = opc1 "linrand" [ k max ] rate

let linseg ?(rate = kr) ?(init = ~$0.0) pairs =
  opc1 "linseg"
    (i init :: List.fold_right (fun (a, b) acc -> i a :: i b :: acc) pairs [])
    rate

let logcurve ~steepness index = opc1 "logcurve" [ k index; k steepness ] kr

let loopseg ?(init_phase = ~$0.0) ~freq ~trig pairs =
  opc1 "loopseg"
    ( k freq :: k trig :: i init_phase
    :: List.fold_right (fun (a, b) acc -> k a :: k b :: acc) pairs [] )
    kr

let loopsegp ~phase pairs =
  opc1 "loopsegp"
    (k phase :: List.fold_right (fun (a, b) acc -> k a :: k b :: acc) pairs [])
    kr

module Looptseg = struct
  let looptseg_line = 0.0

  let looptseg_concave = 1.0

  let looptseg_convex = -1.0
end

let looptseg ?(init_phase = ~$0.0) ~freq ~trig triples =
  opc1 "looptseg"
    ( k freq :: k trig :: k init_phase
    :: List.fold_right
         (fun (a, b, c) acc -> k a :: k b :: k c :: acc)
         triples [] )
    kr

let loopxseg ?(init_phase = ~$0.0) ~freq ~trig pairs =
  opc1 "loopxseg"
    ( k freq :: k trig :: i init_phase
    :: List.fold_right (fun (a, b) acc -> k a :: k b :: acc) pairs [] )
    kr

let lowpass2 ~freq ~q asig = opc1 "lowpass2" [ a asig; k freq; k q ] ar

let lowres ~freq ~reson asig = opc1 "lowres" [ a asig; k freq; k reson ] ar

let lowresx ?(num = ~$4.0) ~freq ~reson asig =
  opc1 "lowresx" [ a asig; k freq; k reson; i num ] ar

let lpf18 ?(dist = ~$0.0) ~freq ~reson asig =
  opc1 "lpf18" [ a asig; freq; reson; dist ] ar

module Lphasor = struct
  let no = 0.0

  let forward = 1.0

  let backward = 2.0

  let forward_backward = 3.0
end

let lphasor ?(loop_start = ~$0.0) ?(loop_end = ~$0.0) ?(mode = ~$Lphasor.no)
    ?(start = ~$0.0) trans =
  opc1 "lphasor" [ trans; i loop_start; i loop_end; i mode; i start ] ar

let lpshold ?(init_phase = ~$0.0) ~freq ~trig pairs =
  opc1 "lpshold"
    ( k freq :: k trig :: i init_phase
    :: List.fold_right (fun (a, b) acc -> k a :: k b :: acc) pairs [] )
    kr

let lpsholdp ~phase pairs =
  opc1 "lpsholdp"
    (k phase :: List.fold_right (fun (a, b) acc -> k a :: k b :: acc) pairs [])
    kr

let mac pairs =
  opc1 "mac" (List.fold_right (fun (x, y) acc -> a x :: k y :: acc) pairs []) ar

let maca pairs =
  opc1 "maca"
    (List.fold_right (fun (x, y) acc -> a x :: a y :: acc) pairs [])
    ar

let madsr ?(rate = kr) ?(delay = ~$0.0) ~attack ~decay ~release level =
  opc1 "madsr" [ i attack; i decay; i level; i release; i delay ] rate

let max ~rate xs = opc1 "max" (List.map (to_rate rate) xs) rate

let maxabs ~rate xs = opc1 "maxabs" (List.map (to_rate rate) xs) rate

module Max_k = struct
  let abs = 0.0

  let max = 1.0

  let min = 2.0

  let avg = 3.0
end

let max_k ~mode ~trigger asig = opc1 "max_k" [ a asig; k trigger; i mode ] kr

let median ?(rate = kr) ~size ~maxsize asig =
  match rate with
  | Ar -> opc1 "median" [ a asig; k size; i maxsize ] ar
  | Kr -> opc1 "median" [ k asig; k size; i maxsize ] ar
  | Ir -> failwith "unsupported"

let metro ?(skip_first = false) freq =
  opc1 "metro" [ k freq; (if skip_first then ~$0.00000001 else ~$0.0) ] kr

let min ~rate xs = opc1 "min" (List.map (to_rate rate) xs) rate

let minabs ~rate xs = opc1 "minabs" (List.map (to_rate rate) xs) rate

let mirror ~rate ~low ~high inp =
  opc1 "mirror" [ to_rate rate inp; to_rate rate low; to_rate rate high ] rate

let mode ~freq ~q asig = opc1 "mode" [ a asig; freq; q ] ar

let moogladder ~freq ~reson asig = opc1 "moogladder" [ a asig; freq; reson ] ar

let moogladder2 ~freq ~reson asig =
  opc1 "moogladder2" [ a asig; freq; reson ] ar

let moogvcf ?(scale = ~$1.0) ~freq ~reson asig =
  opc1 "moogvcf2" [ a asig; freq; reson; i scale ] ar

let mpulse ?(amp = ~$1.0) ?(delay = ~$0.0) period =
  opc1 "mpulse" [ k amp; k period; i delay ] kr

let multitap pairs asig =
  opc1 "multitap"
    (a asig :: List.fold_right (fun (x, y) acc -> i x :: i y :: acc) pairs [])
    ar

let mxadsr ?(rate = kr) ?(delay = ~$0.0) ~attack ~decay ~release level =
  opc1 "mxadsr" [ i attack; i decay; i level; i release; i delay ] rate

let nreverb ~time ~hfdiff asig = opc1 "nreverb" [ a asig; k time; k hfdiff ] ar

let ntrpol ?(min = ~$0.0) ?(max = ~$1.0) ~rate ~point in1 in2 =
  opc1 "ntrpol"
    [
      to_rate rate in1;
      to_rate rate in2;
      (match rate with Ir -> i point | _ -> k point);
      i min;
      i max;
    ]
    rate

module Pareq = struct
  let peaking = 0.0

  let low_shelving = 1.0

  let high_shelving = 2.0
end

let pareq ?(mode = ~$Pareq.peaking) ~v ~freq ~q asig =
  opc1 "pareq" [ asig; k freq; k v; k q; i mode ] ar

let pcauchy ?(rate = kr) alpha = opc1 "pcauchy" [ k alpha ] rate

let peak inp =
  match Signal.rate_opt_of_expr inp with
  | Some Ar -> opc1 "peak" [ inp ] ar
  | Some Kr -> opc1 "peak" [ inp ] kr
  | _ -> opc1 "peak" [ k inp ] kr

let phaser1 ~freq ~stages ~feedback asig =
  opc1 "phaser1" [ a asig; k freq; i stages; k feedback ] ar

module Phaser2 = struct
  let mode1 = 1.0

  let mode2 = 2.0
end

let phaser2 ~freq ~q ~stages ~mode ~sep ~feedback asig =
  opc1 "phaser1" [ a asig; k freq; k q; i stages; i mode; k sep; k feedback ] ar

let phasor ?(init_phase = ~$0.0) ?(rate = ar) freq =
  opc1 "phasor"
    [ (if Signal.rate_equal rate Kr then k freq else freq); i init_phase ]
    rate

let pink = opc1 "pinker" [] ar

let poisson ?(rate = kr) lambda = opc1 "poisson" [ k lambda ] rate

let polynomial xs ain =
  opc1 "polynomial"
    (a ain :: List.fold_right (fun x acc -> k x :: acc) xs [])
    ar

let port ?(init = ~$0.0) ~halftime ksig =
  match Signal.rate_opt_of_expr ksig with
  | Some Ar -> opc1 "portk" [ k ksig; k halftime; i init ] kr
  | Some Kr -> opc1 "portk" [ k ksig; halftime; i init ] kr
  | _ -> opc1 "port" [ k ksig; halftime; i init ] kr

let poscil ?(rate = ar) ?(table = ~$(-1.0)) ?(phase = ~$0.0) ?(amp = ~$1.0) freq
    =
  opc1 "poscil" [ amp; freq; i table; phase ] rate

let pow ?(norm = ~$1.0) ~rate ~exp xin =
  opc1 "pow"
    [
      to_rate rate xin;
      (if Signal.rate_equal rate Ir then i exp else k exp);
      i norm;
    ]
    rate

let powershape ?(max_input_value = ~$1.0) ~exp ain =
  opc1 "powershape" [ a ain; k exp; i max_input_value ] ar

let product xs =
  opc1 "product" (List.fold_right (fun x acc -> a x :: acc) xs []) ar

let ptrack ?(peaks = ~$20.0) ~size ain =
  opc2 "ptrack" [ a ain; i size; i peaks ] kr kr

let noise ?(rate = ar) ?(offset = ~$0.0) amp =
  opc1 "rand" [ amp; ~$0.5; ~$0.0; i offset ] rate

let randh ?(rate = kr) ?(offset = ~$0.0) ~freq amp =
  match rate with
  | Kr -> opc1 "randh" [ k amp; k freq; ~$0.5; ~$0.0; i offset ] rate
  | Ar -> opc1 "randh" [ amp; freq; ~$0.5; ~$0.0; i offset ] rate
  | Ir -> failwith "unsupported"

let randi ?(rate = kr) ?(offset = ~$0.0) ~freq amp =
  match rate with
  | Kr -> opc1 "randi" [ k amp; k freq; ~$0.5; ~$0.0; i offset ] rate
  | Ar -> opc1 "randi" [ amp; freq; ~$0.5; ~$0.0; i offset ] rate
  | Ir -> failwith "unsupported"

let randrange ?(rate = kr) min max =
  match rate with
  | Signal.Ir -> opc1 "random" [ i min; i max ] ir
  | _ -> opc1 "random" [ k min; k max ] rate

let randrangeh ?(rate = kr) ~freq min max =
  opc1 "randomh"
    [ k min; k max; (match rate with Ar -> freq | _ -> k freq); ~$3.0 ]
    rate

let randrangei ?(rate = kr) ~freq min max =
  opc1 "randomi"
    [ k min; k max; (match rate with Ar -> freq | _ -> k freq); ~$3.0 ]
    rate

module Rbjeq = struct
  let lpf = 0.0

  let hpf = 2.0

  let bpf = 4.0

  let brf = 6.0

  let peaking = 8.0

  let lsf = 10.0

  let hsf = 12.0
end

let rbjeq ?(mode = ~$Rbjeq.lpf) ~freq ~level ~q ~s asig =
  opc1 "rbjeq" [ a asig; k freq; k level; k q; k s; i mode ] ar

let reson ?(rate = ar) ?(scale = ~$1.0) ~freq ~bw asig =
  match rate with
  | Ar -> opc1 "reson" [ a asig; freq; bw; i scale ] ar
  | Kr -> opc1 "resonk" [ k asig; k freq; k bw; i scale ] kr
  | _ -> failwith "unsupported"

let resonr ?(scale = ~$1.0) ~freq ~bw asig =
  opc1 "resonr" [ a asig; freq; bw; i scale ] ar

let resonz ?(scale = ~$1.0) ~freq ~bw asig =
  opc1 "resonz" [ a asig; freq; bw; i scale ] ar

let resonx ?(num = ~$4.0) ?(scale = ~$1.0) ~freq ~bw asig =
  opc1 "resonx" [ a asig; freq; bw; i num; i scale ] ar

module Resony = struct
  let octaves = 0.0

  let hertz = 1.0
end

let resony ?(mode = ~$Resony.octaves) ?(num = ~$4.0) ?(scale = ~$1.0) ~freq ~bw
    ~sep asig =
  opc1 "resonx" [ a asig; freq; bw; i num; k sep; i mode; i scale ] ar

let reverb ~time asig = opc1 "reverb" [ a asig; k time ] ar

let reverbsc ?(sample_rate = sample_rate) ?(pitchm = ~$1.0) ~feedback ~lpfreq
    inl inr =
  opc2 "reverbsc"
    [ a inl; a inr; k feedback; k lpfreq; i sample_rate; i pitchm ]
    ar ar

module Rezzy = struct
  let lpf = 0.0

  let hpf = 1.0
end

let rezzy ?(mode = ~$Rezzy.lpf) ~freq ~res ain =
  opc1 "rezzy" [ a ain; freq; res; i mode ] ar

let rms ?(lpfreq = ~$10.0) ain = opc1 "hms" [ a ain; i lpfreq ] ar

let rand_spline ?(rate = kr) ~min ~max ~min_freq ~max_freq () =
  let min, max = match rate with Kr -> (k min, k max) | _ -> (min, max) in
  opc1 "rspline" [ min; max; k min_freq; k max_freq ] rate

let samphold ?(rate = ar) ?(init = ~$0.0) ~gate inp =
  opc1 "samphold" [ to_rate rate inp; to_rate rate gate; i init; ~$0.0 ] rate

let scale ~min ~max inp = opc1 "scale" [ k inp; k min; k max ] kr

let sndloop ~pitch ~trigger ~dur ~fade inp =
  opc2 "sndloop" [ a inp; k pitch; k trigger; i dur; i fade ] ar kr

let squinewave ~clip ~skew freq =
  opc1 "squinewave" [ a freq; a clip; a skew ] ar

let statevar ?(iosamps = ~$3.0) ~freq ~q ain =
  opc4 "statevar" [ a ain; freq; q; i iosamps ] ar ar ar ar

let streson ~freq ~feedback asig =
  opc1 "streson" [ a asig; k freq; k feedback ] ar

let svfilter ?(scale = ~$1.0) ~freq ~q asig =
  opc3 "svfilter" [ a asig; k freq; k q; i scale ] ar ar ar

let ( <~ ) var x = sink "=" [ var; x ]

let dup ?(rate = ar) x = opc1 "=" [ to_rate rate x ] rate
