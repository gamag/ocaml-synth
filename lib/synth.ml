open Containers

type expr = Signal.expr

type 'a block = 'a Signal.Block.t

type rate = Signal.rate = Ir | Kr | Ar

type param_type = Float_t | Table_t

type param = Float of string * float | Table of string * string

module Instr_sig = struct
  type t = {
    defaults : float array;
    param_map : (string, int * param_type) Hashtbl.t;
  }

  let of_spec tables xs =
    let param_map = Hashtbl.create (Array.length xs) in
    let defaults =
      Array.mapi
        (fun i x ->
          match x with
          | Float (param_name, default) ->
              Hashtbl.add param_map param_name (i, Float_t);
              default
          | Table (param_name, default) ->
              Hashtbl.add param_map param_name (i, Table_t);
              float_of_int (tables default))
        xs
    in
    { defaults; param_map }

  let pfield x key = 4 + fst (Hashtbl.find x.param_map key)

  let fill_params { defaults; param_map } tables arr id onset dur keyargs =
    Ctypes.CArray.(
      unsafe_set arr 0 (float_of_int id);
      unsafe_set arr 1 onset;
      unsafe_set arr 2 dur);
    for i = 0 to Array.length defaults - 1 do
      Ctypes.CArray.unsafe_set arr (i + 3) defaults.(i)
    done;
    Array.iter
      (function
        | Float (param_name, value) -> (
            match Hashtbl.find param_map param_name with
            | i, Float_t -> Ctypes.CArray.unsafe_set arr (i + 3) value
            | _ -> failwith "type error"
            | exception Not_found -> () )
        | Table (param_name, value) -> (
            match Hashtbl.find param_map param_name with
            | i, Table_t ->
                Ctypes.CArray.unsafe_set arr (i + 3)
                  (float_of_int (tables value))
            | _ -> failwith "type error"
            | exception Not_found -> () ))
      keyargs;
    Array.length defaults + 3

  let equal a b =
    let exception False in
    if
      Array.equal Float.equal a.defaults b.defaults
      && Hashtbl.length a.param_map = Hashtbl.length b.param_map
    then
      try
        Hashtbl.iter
          (fun param_name (a_index, a_typ) ->
            let b_index, b_typ = Hashtbl.find b.param_map param_name in
            if b_index <> a_index || not (CCEqual.physical a_typ b_typ) then
              raise False)
          a.param_map;
        true
      with
      | Not_found -> false
      | False -> false
    else false
end

type render

type realtime

type render_t = { buffer : Buffer.t; mutable total_duration : float option }

type 'a mode = Realtime : realtime mode | Render : render_t -> render mode

type compiled_instr = { isig : Instr_sig.t; id : int; code : string }

type 'a t = {
  instrs : (string, compiled_instr) Hashtbl.t;
  mutable last_instr_ids : int array;
  controls : (string, unit) Hashtbl.t;
  tables : (string, int) Hashtbl.t;
  mutable last_table_id : int;
  csound : Csoundapi_ffi.csound;
  event_params : float Ctypes.CArray.t;
  mode : 'a mode;
}

let instr_ids_init_f i = i * 1000000

let make (type a) ~max_event_size ~sample_rate ~control_samples ~channels extra
    (mode : a mode) =
  let csound = Csoundapi_ffi.create Ctypes.null in
  ignore (Csoundapi_ffi.set_option csound "-m6");
  ignore (Csoundapi_ffi.set_option csound "-d");
  List.iter (fun opt -> ignore (Csoundapi_ffi.set_option csound opt)) extra;
  ignore
    (Csoundapi_ffi.compile_orc csound
       (Printf.sprintf "sr = %d\nksmps = %d\nnchnls = %d\n0dbfs = 1\n"
          sample_rate control_samples channels));
  {
    instrs = Hashtbl.create 10;
    last_instr_ids = Array.init 5 instr_ids_init_f;
    last_table_id = 0;
    tables = Hashtbl.create 10;
    controls = Hashtbl.create 10;
    csound;
    event_params = Ctypes.CArray.make Ctypes.double (max_event_size + 3);
    mode;
  }

let realtime ?(max_event_size = 20) ~sample_rate ~control_samples ~channels () =
  make ~max_event_size ~sample_rate ~control_samples ~channels
    [ "-odac"; "--realtime" ] Realtime

let render ?(buffer_size = 5096) ?(max_event_size = 20) ~sample_rate
    ~control_samples ~channels ~file () =
  make ~max_event_size ~sample_rate ~control_samples ~channels [ "-o" ^ file ]
    (Render { buffer = Buffer.create buffer_size; total_duration = None })

let destroy x = ignore (Csoundapi_ffi.destroy x.csound)

let start_performance (type a) (x : a t) =
  ignore
    ( match x.mode with
    | Render { buffer; total_duration } ->
        ( match total_duration with
        | Some dur ->
            Buffer.(
              add_string buffer "e ";
              add_string buffer (string_of_float dur);
              add_char buffer '\n')
        | _ -> () );
        Csoundapi_ffi.read_score x.csound (Buffer.contents buffer)
    | _ -> 0 );
  ignore (Csoundapi_ffi.start x.csound);
  ignore (Csoundapi_ffi.perform x.csound)

let reset x =
  Csoundapi_ffi.reset x.csound;
  Hashtbl.clear x.tables;
  Hashtbl.clear x.instrs;
  x.last_table_id <- 0;
  for i = 0 to Array.length x.last_instr_ids - 1 do
    x.last_instr_ids.(i) <- instr_ids_init_f i
  done

let stop x = ignore (Csoundapi_ffi.stop x.csound)

let with_realtime ?(max_event_size = 20) ~sample_rate ~control_samples ~channels
    f =
  let s = realtime ~max_event_size ~sample_rate ~control_samples ~channels () in
  try
    let res = f s in
    stop s;
    destroy s;
    res
  with e ->
    stop s;
    destroy s;
    raise e

let with_render ?(buffer_size = 5096) ?(max_event_size = 20) ~sample_rate
    ~control_samples ~channels ~file f =
  let s =
    render ~buffer_size ~max_event_size ~sample_rate ~control_samples ~channels
      ~file ()
  in
  try
    let res = f s in
    destroy s;
    res
  with e ->
    destroy s;
    raise e

let with_realtime_perf ?(max_event_size = 20) ~sample_rate ~control_samples
    ~channels f =
  let s = realtime ~max_event_size ~sample_rate ~control_samples ~channels () in
  let thread = Thread.create start_performance s in
  try
    let res = f s in
    stop s;
    Thread.join thread;
    destroy s;
    res
  with e ->
    stop s;
    Thread.join thread;
    destroy s;
    raise e

let with_render_perf ?(buffer_size = 5096) ?(max_event_size = 20) ~sample_rate
    ~control_samples ~channels ~file f =
  let s =
    render ~buffer_size ~max_event_size ~sample_rate ~control_samples ~channels
      ~file ()
  in
  try
    let res = f s in
    start_performance s;
    destroy s;
    res
  with e ->
    destroy s;
    raise e

let perform_control_sample x = Csoundapi_ffi.perform_ksmps x.csound = 0

let get_table_id x name =
  try Hashtbl.find x.tables name
  with Not_found ->
    let id = x.last_table_id + 1 in
    x.last_table_id <- id;
    Hashtbl.add x.tables name id;
    id

let new_instr_id x layer =
  let id = x.last_instr_ids.(layer) + 1 in
  x.last_instr_ids.(layer) <- id;
  id

let compile_instr x name (isig : Instr_sig.t) id block =
  let id_str = string_of_int id in
  Buffer.(
    let buf = create 5096 in
    add_string buf "instr ";
    add_string buf id_str;
    add_string buf " // ";
    add_string buf name;
    add_char buf '\n';
    Signal.write_block buf (Hashtbl.find x.tables) (Instr_sig.pfield isig) block;
    add_string buf "endin // ";
    add_string buf name;
    add_char buf '\n';
    contents buf)

module Syntax = struct
  let instr ?(layer = 0) x name args block =
    let isig = Instr_sig.of_spec (Hashtbl.find x.tables) args in
    match Hashtbl.find_opt x.instrs name with
    | None ->
        let id = new_instr_id x layer in
        let code = compile_instr x name isig id block in
        if Csoundapi_ffi.compile_orc x.csound code = 0 then
          Hashtbl.add x.instrs name { isig; code; id }
    | Some { isig = isig'; code = code'; id } ->
        let code = compile_instr x name isig id block in
        if not (Instr_sig.equal isig isig' && String.equal code code') then
          if Csoundapi_ffi.compile_orc x.csound code = 0 then
            Hashtbl.add x.instrs name { isig; code; id }

  let flt name value = Float (name, value)

  let tab name table = Table (name, table)

  let control x name =
    if Hashtbl.mem x.controls name then ()
    else
      let orc_code = Printf.sprintf "chn_k \"%s\", 3, 0\n" name in
      ignore (Csoundapi_ffi.compile_orc x.csound orc_code)

  let get_control x name =
    Csoundapi_ffi.get_control_channel x.csound name Ctypes.null

  let set_control x name value =
    Csoundapi_ffi.set_control_channel x.csound name value

  let sound_file (type a) ?(skip = 0.0) ?(channel = 0) ~file (x : a t) name =
    let id = get_table_id x name in
    let score =
      Printf.sprintf "f %d 0 0 1 \"%s\" %f 0 %d\n" id file skip channel
    in
    match x.mode with
    | Realtime -> Csoundapi_ffi.read_score x.csound score |> ignore
    | Render { buffer; _ } -> Buffer.add_string buffer score

  let play (type a) (x : a t) instr_name onset dur args =
    let { isig; id; _ } = Hashtbl.find x.instrs instr_name in
    if Float.(dur > 0.0) then
      let len =
        Instr_sig.fill_params isig (Hashtbl.find x.tables) x.event_params id
          onset dur args
      in
      match x.mode with
      | Render { buffer; _ } ->
          Buffer.(
            add_char buffer 'i';
            for i = 0 to len - 1 do
              add_char buffer ' ';
              add_string buffer
                (string_of_float (Ctypes.CArray.unsafe_get x.event_params i))
            done;
            add_char buffer '\n')
      | Realtime ->
          ignore
            (Csoundapi_ffi.score_event x.csound 'i'
               (Ctypes.CArray.start x.event_params)
               (Signed.Long.of_int len))

  let total_duration (x : render t) dur =
    match x.mode with Render r -> r.total_duration <- dur

  include Signal.Export
  include Opcodes
end

module Extend = struct
  let sink, sinkn, sink1, sink2, sink3, sink4 =
    Signal.Export.(sink, sinkn, sink1, sink2, sink3, sink4)

  let opcoden, opcode1, opcode2, opcode3, opcode4 =
    Signal.(opcoden, opcode1, opcode2, opcode3, opcode4)
end
