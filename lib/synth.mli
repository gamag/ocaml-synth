type 'a t

type realtime

type render

type rate = Ir | Kr | Ar

type expr

type 'a block

type param = Float of string * float | Table of string * string

val realtime :
  ?max_event_size:int ->
  sample_rate:int ->
  control_samples:int ->
  channels:int ->
  unit ->
  realtime t

val render :
  ?buffer_size:int ->
  ?max_event_size:int ->
  sample_rate:int ->
  control_samples:int ->
  channels:int ->
  file:string ->
  unit ->
  render t

val with_realtime :
  ?max_event_size:int ->
  sample_rate:int ->
  control_samples:int ->
  channels:int ->
  (realtime t -> 'a) ->
  'a

val with_render :
  ?buffer_size:int ->
  ?max_event_size:int ->
  sample_rate:int ->
  control_samples:int ->
  channels:int ->
  file:string ->
  (render t -> 'a) ->
  'a

val with_realtime_perf :
  ?max_event_size:int ->
  sample_rate:int ->
  control_samples:int ->
  channels:int ->
  (realtime t -> 'a) ->
  'a

val with_render_perf :
  ?buffer_size:int ->
  ?max_event_size:int ->
  sample_rate:int ->
  control_samples:int ->
  channels:int ->
  file:string ->
  (render t -> 'a) ->
  'a

val destroy : 'a t -> unit

val perform_control_sample : 'a t -> bool

val start_performance : 'a t -> unit

val stop : 'a t -> unit

val reset : 'a t -> unit

module Syntax : sig
  val instr : ?layer:int -> 'a t -> string -> param array -> unit block -> unit

  val control : realtime t -> string -> unit

  val sound_file :
    ?skip:float -> ?channel:int -> file:string -> 'a t -> string -> unit

  val play : 'a t -> string -> float -> float -> param array -> unit

  val set_control : realtime t -> string -> float -> unit

  val get_control : realtime t -> string -> float

  val total_duration : render t -> float option -> unit

  val flt : string -> float -> param

  val tab : string -> string -> param

  val ( ~$ ) : float -> expr

  val const : float -> expr

  val const_table : string -> expr

  val ( ~@ ) : string -> expr

  val neg : expr -> expr

  val ( + ) : expr -> expr -> expr

  val ( - ) : expr -> expr -> expr

  val ( * ) : expr -> expr -> expr

  val ( / ) : expr -> expr -> expr

  val ( ** ) : expr -> expr -> expr

  val ( mod ) : expr -> expr -> expr

  val ( = ) : expr -> expr -> expr

  val ( <> ) : expr -> expr -> expr

  val ( < ) : expr -> expr -> expr

  val ( > ) : expr -> expr -> expr

  val ( <= ) : expr -> expr -> expr

  val ( >= ) : expr -> expr -> expr

  val einstr : expr

  val eonset : expr

  val edur : expr

  val pi_2 : expr

  val sample_rate : expr

  val sin : expr -> expr

  val cos : expr -> expr

  val tdur : expr -> expr

  val ir : rate

  val kr : rate

  val ar : rate

  val i : expr -> expr

  val k : expr -> expr

  val a : expr -> expr

  val to_rate : rate -> expr -> expr

  val return : 'a -> 'a block

  val bind : 'a block -> ('a -> 'b block) -> 'b block

  val map : 'a block -> ('a -> 'b) -> 'b block

  val if_ : expr -> unit block -> unit block -> unit block

  val ( >>= ) : 'a block -> ('a -> 'b block) -> 'b block

  val ( >|= ) : 'a block -> ('a -> 'b) -> 'b block

  val ( let$ ) : 'a block -> ('a -> 'b block) -> 'b block

  val adsr :
    ?rate:rate ->
    ?delay:expr ->
    att:expr ->
    dec:expr ->
    rel:expr ->
    expr ->
    expr

  val alpass : reverb:expr -> loop:expr -> expr -> expr

  val areson : ?rate:rate -> freq:expr -> bw:expr -> expr -> expr

  val atone : ?rate:rate -> hp:expr -> expr -> expr

  val atonex : ?rate:rate -> ?num:expr -> hp:expr -> expr -> expr

  val balance : expr -> expr -> expr

  val beosc : bw:expr -> expr -> expr

  val betarand :
    ?rate:rate -> range:expr -> alpha:expr -> beta:expr -> unit -> expr

  val bexprnd : ?rate:rate -> expr -> expr

  val biquad :
    ?inrate:rate -> expr -> expr -> expr -> expr -> expr -> expr -> expr -> expr

  module Bqrez : sig
    val lp : float

    val hp : float

    val bp : float

    val br : float

    val ap : float
  end

  val bqrez : ?mode:expr -> ?res:expr -> freq:expr -> expr -> expr

  val butbp : freq:expr -> bw:expr -> expr -> expr

  val butbr : freq:expr -> bw:expr -> expr -> expr

  val buthp : freq:expr -> expr -> expr

  val butlp : freq:expr -> expr -> expr

  val cauchy : ?rate:rate -> expr -> expr

  val cauchyi : ?rate:rate -> ?amp:expr -> freq:expr -> expr -> expr

  val centroid : trig:expr -> fftn:expr -> expr -> expr

  val changed : ?report_first:bool -> expr list -> expr

  val chebyshevpoly : expr -> expr list -> expr -> expr

  val clip : limit:expr -> expr -> expr

  val comb : reverb:expr -> loop:expr -> expr -> expr

  val combinv : reverb:expr -> loop:expr -> expr -> expr

  val compress :
    thresh:expr ->
    lowknee:expr ->
    hiknee:expr ->
    ratio:expr ->
    att:expr ->
    rel:expr ->
    lookahead:expr ->
    control_sig:expr ->
    expr ->
    expr

  val cosseg : ?rate:rate -> ?init:expr -> (expr * expr) list -> expr

  val dam :
    thresh:expr ->
    hiratio:expr ->
    lowratio:expr ->
    rise:expr ->
    fall:expr ->
    expr ->
    expr

  val dcblock : ?order:expr -> expr -> expr

  val diff : ?rate:rate -> expr -> expr

  val downsamp : ?window:expr -> expr -> expr

  val dup : ?rate:rate -> expr -> expr

  val dust : ?rate:rate -> ?with_neg:bool -> amp:expr -> expr -> expr

  val ephasor : decayrate:expr -> expr -> expr * expr

  val eqfil : freq:expr -> bw:expr -> gain:expr -> expr -> expr

  val exciter :
    freq:expr -> ceil:expr -> harm:expr -> blend:expr -> expr -> expr

  val expcurve : steepness:expr -> expr -> expr

  val expon : dur:expr -> expr -> expr -> expr

  val exprand : ?rate:rate -> expr -> expr

  val exprandi : ?rate:rate -> amp:expr -> freq:expr -> expr -> expr

  val expseg : ?rate:rate -> ?init:expr -> (expr * expr) list -> expr

  val flanger : delay:expr -> feedback:expr -> expr -> expr

  val fold : incr:expr -> expr -> expr

  val follow : avgperiod:expr -> expr -> expr

  val follow2 : att:expr -> rel:expr -> expr -> expr

  val fractal_noise : ?beta:expr -> expr -> expr

  val freeverb :
    ?sr:expr -> room_size:expr -> hfdamp:expr -> expr -> expr -> expr * expr

  val gain : rms:expr -> expr -> expr

  val gauss : ?rate:rate -> expr -> expr

  val gaussi : ?rate:rate -> amp:expr -> freq:expr -> expr -> expr

  val gausstrig : ?rate:rate -> ?amp:expr -> expr -> expr -> expr

  val gendy :
    ?rate:rate ->
    ?amp:expr ->
    ?ctrlpts:expr ->
    ?maxctrlpts:expr ->
    ampdist:expr ->
    durdist:expr ->
    adpar:expr ->
    ddpar:expr ->
    minfreq:expr ->
    maxfreq:expr ->
    ascale:expr ->
    dscale:expr ->
    expr

  val gendyc :
    ?rate:rate ->
    ?amp:expr ->
    ?ctrlpts:expr ->
    ?maxctrlpts:expr ->
    ampdist:expr ->
    durdist:expr ->
    adpar:expr ->
    ddpar:expr ->
    minfreq:expr ->
    maxfreq:expr ->
    ascale:expr ->
    dscale:expr ->
    expr

  val gendyx :
    ?rate:rate ->
    ?amp:expr ->
    ?ctrlpts:expr ->
    ?maxctrlpts:expr ->
    ampdist:expr ->
    durdist:expr ->
    adpar:expr ->
    ddpar:expr ->
    minfreq:expr ->
    maxfreq:expr ->
    ascale:expr ->
    dscale:expr ->
    curveup:expr ->
    curvedown:expr ->
    expr

  val hann : ?dur:expr -> expr -> expr

  val hilbert : expr -> expr * expr

  val hilbert2 : fftsize:expr -> hopsize:expr -> expr -> expr * expr

  val init : rate -> expr -> expr

  val integ : ?rate:rate -> expr -> expr

  val interp : ?init:expr -> expr -> expr

  val jitter : minfreq:expr -> maxfreq:expr -> expr -> expr

  val jitter2 :
    comp1:expr * expr -> comp2:expr * expr -> comp3:expr * expr -> expr -> expr

  val jitter_spline : ?rate:rate -> minfreq:expr -> maxfreq:expr -> expr -> expr

  val k35_hpf : freq:expr -> q:expr -> expr -> expr

  val k35_lpf : freq:expr -> q:expr -> expr -> expr

  module Lfo : sig
    val sine : float

    val triangle : float

    val square_bi : float

    val square_uni : float

    val saw : float

    val saw_down : float
  end

  val lfo : ?mode:expr -> ?rate:rate -> ?amp:expr -> expr -> expr

  val limit : ?rate:rate -> low:expr -> high:expr -> expr -> expr

  val line : ?rate:rate -> ?dur:expr -> expr -> expr -> expr

  val linen : ?rate:rate -> ?dur:expr -> rise:expr -> decay:expr -> expr -> expr

  val lineto : time:expr -> expr -> expr

  val linlin : ?rate:rate -> ?dst:expr * expr -> src:expr * expr -> expr -> expr

  val lincos : ?rate:rate -> ?dst:expr * expr -> src:expr * expr -> expr -> expr

  val linrand : ?rate:rate -> expr -> expr

  val linseg : ?rate:rate -> ?init:expr -> (expr * expr) list -> expr

  val logcurve : steepness:expr -> expr -> expr

  val loopseg :
    ?init_phase:expr -> freq:expr -> trig:expr -> (expr * expr) list -> expr

  val loopsegp : phase:expr -> (expr * expr) list -> expr

  val looptseg :
    ?init_phase:expr ->
    freq:expr ->
    trig:expr ->
    (expr * expr * expr) list ->
    expr

  val loopxseg :
    ?init_phase:expr -> freq:expr -> trig:expr -> (expr * expr) list -> expr

  val lowpass2 : freq:expr -> q:expr -> expr -> expr

  val lowres : freq:expr -> reson:expr -> expr -> expr

  val lowresx : ?num:expr -> freq:expr -> reson:expr -> expr -> expr

  val lpf18 : ?dist:expr -> freq:expr -> reson:expr -> expr -> expr

  val lphasor :
    ?loop_start:expr ->
    ?loop_end:expr ->
    ?mode:expr ->
    ?start:expr ->
    expr ->
    expr

  val lpshold :
    ?init_phase:expr -> freq:expr -> trig:expr -> (expr * expr) list -> expr

  val lpsholdp : phase:expr -> (expr * expr) list -> expr

  val mac : (expr * expr) list -> expr

  val maca : (expr * expr) list -> expr

  val madsr :
    ?rate:rate ->
    ?delay:expr ->
    attack:expr ->
    decay:expr ->
    release:expr ->
    expr ->
    expr

  val max : rate:rate -> expr list -> expr

  val maxabs : rate:rate -> expr list -> expr

  module Max_k : sig
    val abs : float

    val max : float

    val min : float

    val avg : float
  end

  val max_k : mode:expr -> trigger:expr -> expr -> expr

  val median : ?rate:rate -> size:expr -> maxsize:expr -> expr -> expr

  val metro : ?skip_first:bool -> expr -> expr

  val min : rate:rate -> expr list -> expr

  val minabs : rate:rate -> expr list -> expr

  val mirror : rate:rate -> low:expr -> high:expr -> expr -> expr

  val mode : freq:expr -> q:expr -> expr -> expr

  val moogladder : freq:expr -> reson:expr -> expr -> expr

  val moogladder2 : freq:expr -> reson:expr -> expr -> expr

  val moogvcf : ?scale:expr -> freq:expr -> reson:expr -> expr -> expr

  val mpulse : ?amp:expr -> ?delay:expr -> expr -> expr

  val multitap : (expr * expr) list -> expr -> expr

  val mxadsr :
    ?rate:rate ->
    ?delay:expr ->
    attack:expr ->
    decay:expr ->
    release:expr ->
    expr ->
    expr

  val nreverb : time:expr -> hfdiff:expr -> expr -> expr

  val ntrpol :
    ?min:expr -> ?max:expr -> rate:rate -> point:expr -> expr -> expr -> expr

  val outp : ?pan:expr -> expr -> unit block

  val outs : expr -> expr -> unit block

  val chnget : string -> expr block

  val chnset : string -> expr -> unit block

  val pan : ?pan:expr -> expr -> expr * expr

  module Pareq : sig
    val peaking : float

    val low_shelving : float

    val high_shelving : float
  end

  val pareq : ?mode:expr -> v:expr -> freq:expr -> q:expr -> expr -> expr

  val pcauchy : ?rate:rate -> expr -> expr

  val peak : expr -> expr

  val phaser1 : freq:expr -> stages:expr -> feedback:expr -> expr -> expr

  module Phaser2 : sig
    val mode1 : float

    val mode2 : float
  end

  val phaser2 :
    freq:expr ->
    q:expr ->
    stages:expr ->
    mode:expr ->
    sep:expr ->
    feedback:expr ->
    expr ->
    expr

  val phasor : ?init_phase:expr -> ?rate:rate -> expr -> expr

  val pink : expr

  val poisson : ?rate:rate -> expr -> expr

  val polynomial : expr list -> expr -> expr

  val port : ?init:expr -> halftime:expr -> expr -> expr

  val poscil :
    ?rate:rate -> ?table:expr -> ?phase:expr -> ?amp:expr -> expr -> expr

  val pow : ?norm:expr -> rate:rate -> exp:expr -> expr -> expr

  val powershape : ?max_input_value:expr -> exp:expr -> expr -> expr

  val product : expr list -> expr

  val ptrack : ?peaks:expr -> size:expr -> expr -> expr * expr

  val noise : ?rate:rate -> ?offset:expr -> expr -> expr

  val randh : ?rate:rate -> ?offset:expr -> freq:expr -> expr -> expr

  val randi : ?rate:rate -> ?offset:expr -> freq:expr -> expr -> expr

  val randrange : ?rate:rate -> expr -> expr -> expr

  val randrangeh : ?rate:rate -> freq:expr -> expr -> expr -> expr

  val randrangei : ?rate:rate -> freq:expr -> expr -> expr -> expr

  module Rbjeq : sig
    val lpf : float

    val hpf : float

    val bpf : float

    val brf : float

    val peaking : float

    val lsf : float

    val hsf : float
  end

  val rbjeq :
    ?mode:expr -> freq:expr -> level:expr -> q:expr -> s:expr -> expr -> expr

  val reson : ?rate:rate -> ?scale:expr -> freq:expr -> bw:expr -> expr -> expr

  val resonr : ?scale:expr -> freq:expr -> bw:expr -> expr -> expr

  val resonz : ?scale:expr -> freq:expr -> bw:expr -> expr -> expr

  val resonx : ?num:expr -> ?scale:expr -> freq:expr -> bw:expr -> expr -> expr

  module Resony : sig
    val octaves : float

    val hertz : float
  end

  val resony :
    ?mode:expr ->
    ?num:expr ->
    ?scale:expr ->
    freq:expr ->
    bw:expr ->
    sep:expr ->
    expr ->
    expr

  val reverb : time:expr -> expr -> expr

  val reverbsc :
    ?sample_rate:expr ->
    ?pitchm:expr ->
    feedback:expr ->
    lpfreq:expr ->
    expr ->
    expr ->
    expr * expr

  module Rezzy : sig
    val lpf : float

    val hpf : float
  end

  val rezzy : ?mode:expr -> freq:expr -> res:expr -> expr -> expr

  val rms : ?lpfreq:expr -> expr -> expr

  val rand_spline :
    ?rate:rate ->
    min:expr ->
    max:expr ->
    min_freq:expr ->
    max_freq:expr ->
    unit ->
    expr

  val samphold : ?rate:rate -> ?init:expr -> gate:expr -> expr -> expr

  val saw : ?amp:expr -> expr -> expr

  val scale : min:expr -> max:expr -> expr -> expr

  val sine : ?rate:rate -> ?amp:expr -> expr -> expr

  val sndloop :
    pitch:expr -> trigger:expr -> dur:expr -> fade:expr -> expr -> expr * expr

  val square : ?amp:expr -> ?pw:expr -> expr -> expr

  val squinewave : clip:expr -> skew:expr -> expr -> expr

  val statevar :
    ?iosamps:expr -> freq:expr -> q:expr -> expr -> expr * expr * expr * expr

  val streson : freq:expr -> feedback:expr -> expr -> expr

  val svfilter :
    ?scale:expr -> freq:expr -> q:expr -> expr -> expr * expr * expr

  val table_sub : ?wrap:expr -> ?rate:rate -> index:expr -> expr -> expr

  val triangle : ?amp:expr -> expr -> expr

  val ( <~ ) : expr -> expr -> unit block
end

module Extend : sig
  val sink : string -> expr list -> unit block

  val sinkn : string -> expr list -> rate list -> expr array block

  val sink1 : string -> expr list -> rate -> expr block

  val sink2 : string -> expr list -> rate -> rate -> (expr * expr) block

  val sink3 :
    string -> expr list -> rate -> rate -> rate -> (expr * expr * expr) block

  val sink4 :
    string ->
    expr list ->
    rate ->
    rate ->
    rate ->
    rate ->
    (expr * expr * expr * expr) block

  val opcoden : string -> expr list -> rate list -> expr array

  val opcode1 : string -> expr list -> rate -> expr

  val opcode2 : string -> expr list -> rate -> rate -> expr * expr

  val opcode3 :
    string -> expr list -> rate -> rate -> rate -> expr * expr * expr

  val opcode4 :
    string ->
    expr list ->
    rate ->
    rate ->
    rate ->
    rate ->
    expr * expr * expr * expr
end
