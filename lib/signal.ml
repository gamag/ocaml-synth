open Containers

module Id : sig
  type t

  val gen : unit -> t

  val to_string : t -> string

  val compare : t -> t -> int

  val equal : t -> t -> bool
end = struct
  type t = int

  let gen =
    let next = ref 0 in
    fun () ->
      let x = !next in
      incr next;
      x

  let compare = Int.compare

  let equal = Int.equal

  let to_string = string_of_int
end

module Ids = Set.Make (Id)

type rate = Ir | Kr | Ar

type binop = Add | Sub | Mul | Div | Pow | Mod | Eq | Ne | Gt | Lt | Ge | Le

type expr =
  | Lit_table of string
  | Lit of float
  | Lit_string of string
  | Const of string
  | P of string
  | Var of opcode * int
  | Gvar of opcode * int
  | Neg of expr
  | Func of rate option * string * expr list
  | Binop of rate option * binop * expr * expr

and opcode = {
  id : Id.t;
  opcode : string;
  input : expr list;
  output : rate array;
}

type cmd = Opc of opcode | If of Id.t * expr * cmd list * cmd list

let rate_equal : rate -> rate -> bool = Stdlib.( = )

let cmd_id = function Opc o -> o.id | If (id, _, _, _) -> id

module Block : sig
  type 'a t

  val return : 'a -> 'a t

  val bind : 'a t -> ('a -> 'b t) -> 'b t

  val map : 'a t -> ('a -> 'b) -> 'b t

  val if_ : expr -> unit t -> unit t -> unit t

  val sink : string -> expr list -> unit t

  val sinkn : string -> expr list -> rate list -> expr array t

  val sink1 : string -> expr list -> rate -> expr t

  val sink2 : string -> expr list -> rate -> rate -> (expr * expr) t

  val sink3 :
    string -> expr list -> rate -> rate -> rate -> (expr * expr * expr) t

  val sink4 :
    string ->
    expr list ->
    rate ->
    rate ->
    rate ->
    rate ->
    (expr * expr * expr * expr) t

  val to_list : 'a t -> cmd list

  val output : 'a t -> 'a
end = struct
  type 'a t = Ids.t * cmd list * 'a

  let output (_, _, x) = x

  let return x = (Ids.empty, [], x)

  let bind (set, ctx, x) f =
    let set', ctx', x' = f x in
    let ctx'' =
      List.fold_right
        (fun x acc ->
          match x with
          | _ when Ids.mem (cmd_id x) set -> acc
          | Opc _ -> x :: acc
          | If (_id, tm, then_, else_) ->
              If
                ( Id.gen (),
                  tm,
                  List.take_while (fun x -> not (Ids.mem (cmd_id x) set)) then_,
                  List.take_while (fun x -> not (Ids.mem (cmd_id x) set)) else_
                )
              :: acc)
        ctx ctx'
    in
    (Ids.union set' set, ctx'', x')

  let map (set, ctx, x) f = (set, ctx, f x)

  let rec add_cmd ((set, cmds) as p) cmd =
    if Ids.mem (cmd_id cmd) set then (set, cmds)
    else
      match cmd with
      | Opc o ->
          let set, cmds = add_opcodes_list p o.input in
          (Ids.add (cmd_id cmd) set, cmd :: cmds)
      | If (_, t, then_, else_) ->
          let p = add_opcodes_expr p t in
          let p = add_cmd_list p then_ in
          let set, cmds = add_cmd_list p else_ in
          (Ids.add (cmd_id cmd) set, cmd :: cmds)

  and add_opcodes_expr p = function
    | Var (o, _) -> add_cmd p (Opc o)
    | Neg x -> add_opcodes_expr p x
    | Func (_, _, args) -> add_opcodes_list p args
    | Binop (_, _, a, b) -> add_opcodes_expr (add_opcodes_expr p a) b
    | _ -> p

  and add_cmd_list init xs = List.fold_left add_cmd init xs

  and add_opcodes_list init xs = List.fold_left add_opcodes_expr init xs

  let to_list (_, ctx, _) = List.rev ctx

  let list_split_while f xs =
    let rec loop acc = function
      | hd :: tl when f hd -> loop (hd :: acc) tl
      | t -> (List.rev acc, t)
    in
    loop [] xs

  let if_ t (s1, c1, ()) (s2, c2, ()) =
    let ((set, _cmds) as p) = add_opcodes_expr (Ids.empty, []) t in
    let then_cmds, then_common =
      list_split_while
        (fun x -> not (Ids.mem (cmd_id x) s2 || Ids.mem (cmd_id x) set))
        c1
    in
    let else_cmds, else_common =
      list_split_while
        (fun x -> not (Ids.mem (cmd_id x) s1 || Ids.mem (cmd_id x) set))
        c2
    in
    let p = add_cmd_list p then_common in
    let set, common = add_cmd_list p else_common in
    let id = Id.gen () in
    (Ids.add id set, If (id, t, then_cmds, else_cmds) :: common, ())

  let of_opcode o =
    let set, cmds = add_cmd (Ids.empty, []) (Opc o) in
    (set, cmds, ())

  let sink opcode input =
    of_opcode { id = Id.gen (); opcode; input; output = [||] }

  let sinkn opcode input output =
    let o = { id = Id.gen (); opcode; input; output = Array.of_list output } in
    map (of_opcode o) (fun () ->
        Array.(init (length o.output) (fun i -> Var (o, i))))

  let sink1 opcode input out0 =
    let o = { id = Id.gen (); opcode; input; output = [| out0 |] } in
    map (of_opcode o) (fun () -> Var (o, 0))

  let sink2 opcode input out0 out1 =
    let o = { id = Id.gen (); opcode; input; output = [| out0; out1 |] } in
    map (of_opcode o) (fun () -> (Var (o, 0), Var (o, 1)))

  let sink3 opcode input out0 out1 out2 =
    let o =
      { id = Id.gen (); opcode; input; output = [| out0; out1; out2 |] }
    in
    map (of_opcode o) (fun () -> (Var (o, 0), Var (o, 1), Var (o, 2)))

  let sink4 opcode input out0 out1 out2 out3 =
    let o =
      { id = Id.gen (); opcode; input; output = [| out0; out1; out2; out3 |] }
    in
    map (of_opcode o) (fun () ->
        (Var (o, 0), Var (o, 1), Var (o, 2), Var (o, 3)))
end

let opcoden opcode input output =
  let o = { id = Id.gen (); opcode; input; output = Array.of_list output } in
  Array.init (Array.length o.output) (fun i -> Var (o, i))

let opcode1 opcode input output =
  let o = { id = Id.gen (); opcode; input; output = [| output |] } in
  Var (o, 0)

let opcode2 opcode input out0 out1 =
  let o = { id = Id.gen (); opcode; input; output = [| out0; out1 |] } in
  (Var (o, 0), Var (o, 1))

let opcode3 opcode input out0 out1 out2 =
  let o = { id = Id.gen (); opcode; input; output = [| out0; out1; out2 |] } in
  (Var (o, 0), Var (o, 1), Var (o, 2))

let opcode4 opcode input out0 out1 out2 out3 =
  let o =
    { id = Id.gen (); opcode; input; output = [| out0; out1; out2; out3 |] }
  in
  (Var (o, 0), Var (o, 1), Var (o, 2), Var (o, 3))

let rec rate_opt_of_expr = function
  | Lit_string _ -> Some Ir
  | Lit_table _ | Lit _ | Const _ | P _ -> None
  | Var (o, i) | Gvar (o, i) -> Some o.output.(i)
  | Neg x -> rate_opt_of_expr x
  | Func (r, _, _) | Binop (r, _, _, _) -> r

let rate_opt_max a b =
  match (a, b) with
  | None, x | x, None -> x
  | Some x, Some y -> (
      match (x, y) with
      | Ar, _ | _, Ar -> Some Ar
      | Kr, _ | _, Kr -> Some Kr
      | _, _ -> Some Ir )

let rate_opt_max_of_expr a b =
  rate_opt_max (rate_opt_of_expr a) (rate_opt_of_expr b)

module Export = struct
  let ( ~$ ) x = Lit x

  let ( ~@ ) x = P x

  let const x = Lit x

  let param x = P x

  let const_table table = Lit_table table

  let const_string str = Lit_string str

  let neg x = Neg x

  let ( + ) a b = Binop (rate_opt_max_of_expr a b, Add, a, b)

  let ( - ) a b = Binop (rate_opt_max_of_expr a b, Sub, a, b)

  let ( * ) a b = Binop (rate_opt_max_of_expr a b, Mul, a, b)

  let ( / ) a b = Binop (rate_opt_max_of_expr a b, Div, a, b)

  let ( ** ) a b = Binop (rate_opt_max_of_expr a b, Pow, a, b)

  let ( mod ) a b = Binop (rate_opt_max_of_expr a b, Mod, a, b)

  let ( = ) a b = Binop (rate_opt_max_of_expr a b, Eq, a, b)

  let ( <> ) a b = Binop (rate_opt_max_of_expr a b, Ne, a, b)

  let ( < ) a b = Binop (rate_opt_max_of_expr a b, Lt, a, b)

  let ( > ) a b = Binop (rate_opt_max_of_expr a b, Gt, a, b)

  let ( <= ) a b = Binop (rate_opt_max_of_expr a b, Le, a, b)

  let ( >= ) a b = Binop (rate_opt_max_of_expr a b, Ge, a, b)

  let einstr = Const "p1"

  let eonset = Const "p2"

  let edur = Const "p3"

  let pi_2 = Const "$M_PI_2"

  let sample_rate = Const "sr"

  let sin x = Func (rate_opt_of_expr x, "sin", [ x ])

  let cos x = Func (rate_opt_of_expr x, "cons", [ x ])

  let tdur x = Binop (Some Ir, Div, Func (Some Ir, "nsamp", [ x ]), sample_rate)

  let ar = Ar

  let kr = Kr

  let ir = Ir

  let i x =
    match rate_opt_of_expr x with
    | None | Some Ir -> x
    | Some _ -> Func (Some Ir, "i", [ x ])

  let k x =
    match rate_opt_of_expr x with
    | None | Some Kr -> x
    | Some _ -> Func (Some Kr, "k", [ x ])

  let a x =
    match rate_opt_of_expr x with
    | None | Some Ar -> x
    | Some _ -> Func (Some Ar, "a", [ x ])

  let to_rate rate x = match rate with Ar -> a x | Kr -> k x | Ir -> i x

  include Block

  let ( >>= ) = Block.bind

  let ( >|= ) = Block.map

  let ( let$ ) = ( >>= )
end

let write_var ?(global = false) buf o i =
  let open Buffer in
  if global then add_char buf 'g';
  add_char buf (match o.output.(i) with Ir -> 'i' | Kr -> 'k' | Ar -> 'a');
  add_string buf (Id.to_string o.id);
  add_char buf '_';
  add_string buf (string_of_int i)

let rec write_expr buf tables pfields =
  let open Buffer in
  function
  | Lit_string s ->
      add_char buf '"';
      add_string buf s;
      add_char buf '"'
  | Lit_table name -> add_string buf (string_of_int (tables name))
  | Lit x -> add_string buf (string_of_float x)
  | P x ->
      add_char buf 'p';
      add_string buf (pfields x |> string_of_int)
  | Var (o, i) -> write_var buf o i
  | Gvar (o, i) -> write_var ~global:true buf o i
  | Neg x ->
      add_string buf "(-(";
      write_expr buf tables pfields x;
      add_string buf "))"
  | Const s -> add_string buf s
  | Func (_, f, args) ->
      add_string buf f;
      add_char buf '(';
      ( match args with
      | [] -> ()
      | hd :: tl ->
          write_expr buf tables pfields hd;
          List.iter
            (fun e ->
              add_char buf ',';
              write_expr buf tables pfields e)
            tl );
      add_char buf ')'
  | Binop (_, op, a, b) ->
      let op_s =
        match op with
        | Add -> ") + ("
        | Sub -> ") - ("
        | Mul -> ") * ("
        | Div -> ") / ("
        | Pow -> ") ^ ("
        | Mod -> ") % ("
        | Eq -> ") == ("
        | Ne -> ") != ("
        | Gt -> ") > ("
        | Lt -> ") < ("
        | Ge -> ") >= ("
        | Le -> ") <= ("
      in
      add_string buf "((";
      write_expr buf tables pfields a;
      add_string buf op_s;
      write_expr buf tables pfields b;
      add_string buf "))"

let write_opcode ?(indent = "") buf tables pfields
    ({ id = _; opcode; input; output } as o) =
  let open Buffer in
  add_string buf indent;
  match (opcode, output) with
  | "=", [||] ->
      write_expr buf tables pfields (List.hd input);
      add_string buf " = ";
      write_expr buf tables pfields (List.nth input 1)
  | _ ->
      for i = 0 to Array.length output - 1 do
        if i <> 0 then add_string buf ", ";
        write_var buf o i
      done;
      add_char buf ' ';
      add_string buf opcode;
      add_char buf ' ';
      List.iteri
        (fun i x ->
          if i <> 0 then add_string buf ", ";
          write_expr buf tables pfields x)
        input

let rec write_cmd ?(indent = "") buf tables pfield_map =
  let open Buffer in
  function
  | Opc o -> write_opcode ~indent buf tables pfield_map o
  | If (_, tm, c1, c2) ->
      add_string buf indent;
      add_string buf "if ";
      write_expr buf tables pfield_map tm;
      add_string buf " then\n";
      let indent' = indent ^ "  " in
      List.iter
        (fun x ->
          write_cmd ~indent:indent' buf tables pfield_map x;
          add_char buf '\n')
        (List.rev c1);
      ( match c2 with
      | [] -> ()
      | _ ->
          add_string buf indent;
          add_string buf "else\n";
          List.iter
            (fun x ->
              write_cmd ~indent:indent' buf tables pfield_map x;
              add_char buf '\n')
            (List.rev c2) );
      add_string buf indent;
      add_string buf "endif"

let write_block buf tables pfield_map block =
  Block.to_list block
  |> List.iter (fun cmd ->
         Buffer.(
           write_cmd buf tables pfield_map cmd;
           add_char buf '\n'))
