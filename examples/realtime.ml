let () =
  let open Synth in
  (*
  let s = realtime ~sample_rate:48000 ~control_samples:10 ~channels:2 () in
  let thread = Thread.create start_performance s in
  Syntax.(
    instr s "sine" [| flt "freq" 440. |] (outp (sine ~rate:ar ~@"freq" * ~$0.5));
    for _ = 1 to 10 do
      play s "sine" 0. 0.1 [| flt "freq" @@ (440. +. Random.float 220.) |];
      Unix.sleepf 0.2
    done);
  input_line stdin |> ignore;
  stop s;
  Thread.join thread;
  destroy s
    *)
  with_realtime_perf ~sample_rate:48000 ~control_samples:10 ~channels:2
    (fun s ->
      let open Syntax in
      control s "testabc";
      instr s "sine"
        [| flt "freq" 440. |]
        (let$ amp = chnget "testabc" in
         let sine = sine ~rate:ar ~@"freq" * amp in
         outp sine);
      set_control s "testabc" 0.5;
      play s "sine" 0. 1. [| flt "freq" 770. |];
      get_control s "testabc" |> string_of_float |> prerr_endline;
      Unix.sleepf 1.0;
      set_control s "testabc" 0.1;
      play s "sine" 0. 1. [| flt "freq" 770. |];
      get_control s "testabc" |> string_of_float |> prerr_endline;
      Unix.sleepf 1.0;
      set_control s "testabc" 0.8;
      play s "sine" 0. 1. [| flt "freq" 770. |];
      get_control s "testabc" |> string_of_float |> prerr_endline;
      Unix.sleepf 1.0)
