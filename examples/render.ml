let () =
  let open Synth in
  let s =
    render ~buffer_size:1024 ~sample_rate:48000 ~control_samples:10 ~channels:2
      ~file:"test.wav" ()
  in
  Syntax.(
    instr s "sine" [| flt "freq" 440. |] (outp (sine ~rate:ar ~@"freq" * ~$0.5));
    play s "sine" 0. 2. [||];
    total_duration s (Some 2.));
  start_performance s;
  destroy s;
  with_render_perf ~sample_rate:48000 ~control_samples:10 ~channels:2
    ~file:"test2.wav" (fun s ->
      Syntax.(
        sound_file s "sine" ~file:"test.wav";
        instr s "sine"
          [| flt "freq" 440. |]
          (outp (sine ~rate:ar ~@"freq" * ~$0.5));
        instr s "file"
          [| tab "table" "sine" |]
          (outp
             ( ~$0.1
             * table_sub
                 ~index:
                   (phasor ~rate:ar ~init_phase:~$0. (~$1. / tdur ~@"table"))
                 ~@"table" ));
        play s "sine" 0. 2. [| flt "freq" 770. |];
        play s "file" 4. 2. [| tab "table" "sine" |]))
